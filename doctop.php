
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

  <link rel="stylesheet" href="custom.css">
<link rel="stylesheet" href="clearfix.css">
<link rel="stylesheet" href="css/over.css">
<link rel="stylesheet" href="css/shop-view.css">

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>9.49 Tees | 49er Shops Bookstore</title>

<style type="text/css" media="all">
	@import url("http://www.fortyninershops.net/innerweb/v3.0/styles/campushub.css");
	@import url("http://www.fortyninershops.net/innerweb/v3.0/styles/delta/delta.css");
</style>
<script type="text/javascript" src="http://www.fortyninershops.net/innerweb/v3.0/include/js/campushub.js"></script>
<script type="text/javascript" src="http://www.fortyninershops.net/innerweb/v3.0/include/js/jquery.js"></script>
<script type="text/javascript" src="http://www.fortyninershops.net/innerweb/v3.0/include/js/modals.js"></script>
<script type="text/javascript" src="http://www.fortyninershops.net/innerweb/v3.0/include/js/jqmodal.js"></script>
<script type="text/javascript" src="http://www.fortyninershops.net/innerweb/v3.0/include/js/set_modal_div_and_cart_summary_values.js"></script>

<script type="text/javascript" src="http://www.fortyninershops.net/scripts/dt-styleswitcher.js"></script><script type="text/javascript" src="http://www.fortyninershops.net/scripts/BreadCrumbs.js"></script><script type="text/javascript" src="http://www.fortyninershops.net/scripts/ut_menu.js"></script><script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script type="text/javascript">


 function doLoad(){
	autoLinkInit();
 }
 window.onload = doLoad
</script>

<!-- Begin University Template stylesheets -->
<link href="http://www.fortyninershops.net/stylesheets/ut-print.css" rel="stylesheet" type="text/css" media="print" />
<link href="http://www.fortyninershops.net/stylesheets/ut-medium.css" rel="stylesheet" type="text/css" />
<link rel="alternate stylesheet" type="text/css" media="screen,print" href="http://www.fortyninershops.net/stylesheets/ut-small.css" title="small_text" />
<link rel="alternate stylesheet" type="text/css" media="screen,print" href="http://www.fortyninershops.net/stylesheets/ut-medium.css" title="medium_text" />
<link rel="alternate stylesheet" type="text/css" media="screen,print" href="http://www.fortyninershops.net/stylesheets/ut-large.css" title="large_text" />
<link href="http://www.fortyninershops.net/stylesheets/ut.css" rel="stylesheet" type="text/css" media="screen" />
<link href="http://www.fortyninershops.net/stylesheets/website.css" rel="stylesheet" type="text/css" />

<link href="http://www.fortyninershops.net/stylesheets/onecolnav.css" media="screen" rel="stylesheet" type="text/css" />

<link href="http://www.fortyninershops.net/assets/images/favicon.ico" rel="shortcut icon" />
<!-- End University Template stylesheets -->

<link type="text/css" href="http://www.fortyninershops.net/stylesheets/rotator.css" rel="stylesheet" />

<style type="text/css" media="all">
@import url("http://www.fortyninershops.net/stylesheets/customize.css");
</style>
<!-- LocalRef:234 -->

<script type="text/javascript" src="http://www.fortyninershops.net//Innerweb/v3.0/include/js/cookies.js"></script>
<script type="text/javascript">
<!--
var referrer;
var rootURL = 'http://www.fortyninershops.net/';
var rootSURL = 'https://www.fortyninershops.net/';

if (cookies_enabled) {

referrer = getCookie('referring_url');
if (!referrer) {
    referrer = '';
if (referrer == ''){
referrer = 'None';
}else if((referrer.indexOf(rootURL) >= 0) || (referrer.indexOf(rootSURL) >= 0)){
referrer = 'Local';
}
setCookie('referring_url', referrer);
referrer = getCookie('referring_url');
if(!referrer){
referrer = 'Unknown';
}
}else{
    var referring_page = '';
if ((referring_page.indexOf(rootURL) >= 0) ||(referring_page.indexOf(rootSURL) >= 0) ||(referring_page == '')) {
} else {
referrer = referring_page
setCookie('referring_url', referrer);
}
}
} else {
referrer = 'Unknown';
}
//-->
</script>



<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7347222-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">
function validateForm(frm) {

       var formIsValid = true;



       // find all elements that contain the class "validate-*"

      $("*[class*='validate-'][disabled='']",frm).each(function() {

            formIsValid = (validateField(this)) ? formIsValid : false;

        });

        

        $("*[class*='validateconditional-'][disabled='']",frm).each(function() {

            formIsValid = (validateConditionalField(this)) ? formIsValid : false;

        });   

        

    

       

       return formIsValid;

   }

</script>
</head>
