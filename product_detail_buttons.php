<?php include("doctop.php"); ?>

<body  id=shop>

	<div class="clear_float"><a href="#localnav">Skip to Local Navigation</a></div>
<div class="clear_float"><a href="#content">Skip to Content</a></div>
<div id="contain_header">
  <div id="contain_header_setwidth">
    <div id="contain_siteid">
      <div id="siteid_content">
      	<a href="http://www.csulb.edu/" target="_blank"><img src="http://www.fortyninershops.net/assets/UT/hdr_blk_id.gif" alt="California State University, Long Beach" width="287" height="50" border="0" title="California State University, Long Beach" /></a>
       </div>
    </div>
    <div id="contain_search">
      <div id="content_searchtools">
      <ul id="searchtools_links">
	  <li><div id="gSearchbox">
    <form action="http://www.fortyninershops.net/search_advanced_results.asp?" method="post" name="siteSearch" id="frm-sitesearch">
        <label for="txtSearch"><span>search</span><img src="/innerweb/v3.0/styles/delta/images/search_icon.png" alt="search" height="15" width="15" /></label>
        <input name="search_text" id="txtSearch" type="text" class="box" maxlength="50" value=""/>&nbsp;
        <select name="search_scope" id="search_scope" class="box">
            <option value="All">All Products</option>
            <option value="Textbooks">Textbooks</option>
            <option value="Merchandise" selected>Merchandise</option>
            <option value="General Books">General Books</option>
        </select>&nbsp;
        <input type="hidden" name="pageaction" value="redirect"/>
        <!--<input type="submit" value="GO" class="button" />-->
		<input type="image" value="GO" class="sitesearchbutton" src="http://www.fortyninershops.net/assets/interface/searchbutton_icon.gif" alt="Search" />
    </form>
</div>
</li>
    </ul>
    </div>
  </div>
  <div class="clear_float"></div>
</div>
</div>
<div id="contain_divisionnav">
  	<div id="divisionnav_content">
    	<ul id="division_links">
			<li class="phome"><a href="http://www.csulb.edu/aux/49ershops/corporate/">Forty-Niner Shops</a></li>
     		<li class="phome"><a href="http://www.fortyninershops.net/default.asp?">Bookstore Home</a></li>
            
            <!-- Changed for clear serparation of textbook options 2013.06.17, david -->
            <span style="color:#fff;display:inline-block; margin: 0 10px 0 7px;">|</span>
            <li class="phome" style="color:#fff;font-weight:bold;margin-right:5px;">Textbooks:</li>
            <!-- disabled WebPRISM Buy link in favor of VerbaCompare  2011.07.17, ali
            <li class="pbuy"><a href="http://www.fortyninershops.net/buy_main.asp?" title="Buy Textbooks">Buy</a></li> -->
            
            <li class="pbuy"><a href="http://csulb.verbacompare.com/">Buy/Rent</a></li>
            
            
                <li class="psell"><a href="http://www.fortyninershops.net/sell_main.asp?" title="Textbook Buyback: Sell Your Textbooks">Sell</a></li>
            
            
            <!-- Disabled by Ali on 12.12.2010 Per Fred and Rosa's Request
            < if BuybackClassifiedsEnabled=1 then %>
                <li class="pswap"><a href="<= baseURL("swap_main.asp")%>" title="Swap your textbooks with other students">Swap</a></li>
            < end if %>

            -->
            
            <!-- Disabled Chegg in favor of VerbaCompare 7/18/2011, ali
            <li class="phome"><a href="http://www.shopthebeach.com/go/chegg" title="Rent Textbooks & Save Hundreds" target="_blank">Rent</a></li> -->
            
            <li class="phome"><a href="http://www.fortyninershops.net/site_faculty.asp?" title="Faculty Resource Center"> Faculty</a></li>

    	</ul>
	</div>
  </div> 
	<div id="contain_banner_secondary">
  		<a href="http://www.fortyninershops.net/default.asp?">
  			<img src="http://www.fortyninershops.net/assets/banners/banner_university_bookstore.jpg" alt="49er Shops Bookstore" title="49er Shops Bookstore" width="900" height="67" border="0" />
  		</a>
	</div>

<!-- Edited with textwrangler  -->
<a href="#content" title="skip navigation" class="accessibility">skip to main content</a><div id="contain_toolset">
<div id="contain_toolbar">
  <!-- ***** Breadcrumbs container -->
  <div id="contain_breadcrumbs">
    <div id="breadcrumbs_content">
		<!-- BreadCrumbs usage: BreadCrumbs("homePage","sepChars","linkHome",UToSpace,DToSpace,changeCaps,hideExt); -->
        <!-- homePage = "HOME";			// text name for home page link
		 sepChars = " &gt; ";		// character(s) to sepCharsarate links
		 linkHome = "/";			// base URL for links (for subfolders)
		 UToSpace = true;			// change all underscores to spaces in folder names
		 DToSpace = true;			// change all dashes to spaces in folder names
		 changeCaps = 0;			// 0 = no change, 1 = Initial Caps, 2 = All Upper, 3 = All Lower
		 hideExt = true;			// hide extenion in file name -->
        <!--<script type="text/javascript">BreadCrumbs("Home"," &#8250; ","",true,true,1,true);</script>
        <noscript>
        	Please enable JavaScript in your browser.
        </noscript>-->
        <!-- Social Networking Icons -->
        <!--<img src="http://www.fortyninershops.net/assets/interface/vert_divider_2px.gif" alt="Vertical Divider" width="2" height="17" />-->
        <a href="http://www.facebook.com/CSULBbookstore" title="Like Us on Facebook" target="_blank"><img src="http://www.fortyninershops.net/assets/icons/social/facebook_icon_17x17.png" alt="Like Us on Facebook" width="17" height="17" /></a>
        <a href="http://twitter.com/csulbbookstore" title="Follow Us on Twitter" target="_blank"><img src="http://www.fortyninershops.net/assets/icons/social/twitter_icon_17x17.png" alt="Follow Us on Twitter" width="17" height="17" /></a>
        <!-- End Social Networking Icons -->
	</div>
  </div>
  <!-- Container for Tools -->
  <div id="contain_tools">
    <!-- Container for Tools Content -->
    <div id="tools_content">
        <a href="#" onclick="window.print();"><img src="http://www.fortyninershops.net/assets/icons/tool_print.gif" alt="Print this page" width="21" height="17" /></a>
        <a href="#" onclick="addToFavorites();"><img src="http://www.fortyninershops.net/assets/icons/tool_favorite.gif" alt="Add this page to your favorites" width="21" height="17" /></a>
        <!-- Text Size Controls -->
        <img src="http://www.fortyninershops.net/assets/icons/tool_fontsizelabel.gif" alt="Select a font size" width="58" height="17" />
        <a href="#" onclick="setActiveStyleSheet('small_text', 1);"><img src="http://www.fortyninershops.net/assets/icons/tool_small.gif" alt="Select a small font" width="17" height="17" /></a>
        <a href="#" onclick="setActiveStyleSheet('medium_text', 1);"><img src="http://www.fortyninershops.net/assets/icons/tool_medium.gif" alt="Select a medium font" width="17" height="17" /></a>
        <a href="#" onclick="setActiveStyleSheet('large_text', 1);"><img src="http://www.fortyninershops.net/assets/icons/tool_large.gif" alt="Select a large font" width="17" height="17" /></a>
        <!-- End Text Size Controls -->
	</div>
	<!-- ***** Breadcrumbs_home area ends here ***** -->
  </div>
  <!-- ***** End Printable ***** -->
  <!-- Clear floats so that standards compliant browsers display the background color -->
  <div class="clear_float">&nbsp;</div>
</div>
</div>
    
    <div id="container">
        <div id="contain_content">
          <a name="localnav" id="localnav"></a>
      
    <div id="contain_column1">
		<ul>
  <li class="home"><a href="http://www.fortyninershops.net/default.asp?">Home</a></li>
</ul>
	<h2 class="menudivider">Account Info</h2>
	<div id="user-summary">you are not logged in<br /><a href="https://www.fortyninershops.net/account_login.asp?">login</a></div>
	<div class="clear_float"></div>
    			<div id="cart-summary">				<strong>My Cart</strong><br /><span id="cartSummaryValues">0 items</span><br/><span id="cartSummaryLink"></span><br/></div>

	<div class="clear_float"></div>
	<ul>
      	<li class="orders"><a href="http://www.fortyninershops.net/account_orderhistory.asp?">Order History</a></li> 
	</ul><h2 class="menudivider">Textbooks</h2>
    <ul>
        <!-- disabled WebPRISM Buy page in favor of VerbeCompare, 2011.07.18, ali
        <li class="buy"><a href="http://www.fortyninershops.net/buy_main.asp?" title="Buy Textbooks">Buy/Rent</a></li> -->
        
        <li class="buy"><a href="http://csulb.verbacompare.com">Buy/Rent</a></li>
        
        <!--Rent Link Disabled
        <li class="phome"><a href="http://www.shopthebeach.com/go/chegg" title="Rent Textbooks & Save Hundreds" target="_blank">Rent</a></li>
        -->
        
        
		<li class="sell"><a href="http://www.fortyninershops.net/sell_main.asp?">Sell</a></li>
        
		<li class="trade"><a href="http://www.fortyninershops.net/swap_main.asp?">Trade</a></li>
		

        <li><a href="http://www.fortyninershops.net/site_textbook_options_dare_to_compare.asp?">Textbook Options</a></li>   

        <li><a href="http://www.fortyninershops.net/site_csu_digital_rental.asp?">Digital Rentals</a></li>       
    </ul>

<h2 class="menudivider">Shop The Beach</h2>
	
<div id="sub-nav">
<h2>Browse</h2><h3>Clothing</h3><ul><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=701&catalog_name=Mk5kIFN0cmVldCBTcG90bGlnaHQ">2Nd Street Spotlight</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz">9.49 Tees</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=655&catalog_name=QWx1bW5p">Alumni</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=648&catalog_name=QmVhY2ggR2lybA">Beach Girl</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=683&catalog_name=RGlydGJhZ3M">Dirtbags</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=654&catalog_name=RmFtaWx5">Family</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=670&catalog_name=Rm9vdHdlYXI">Footwear</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=646&catalog_name=SGVhZHdlYXI">Headwear</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=678&catalog_name=SmVyc2V5cw">Jerseys</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=687&catalog_name=S2lkcw">Kids</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=702&catalog_name=TmlrZQ">Nike</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=653&catalog_name=T3V0ZXJ3ZWFy">Outerwear</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=652&catalog_name=UG9sb3M">Polos</a></li><li class="selected"><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=681&catalog_name=U2hvcnRzL1BhbnRz">Shorts/Pants</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=705&catalog_name=U2FsZQ">Sale</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=647&catalog_name=U3dlYXRzaGlydHM">Sweatshirts</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=645&catalog_name=VC1TaGlydHM">T-Shirts</a></li></ul>
</div><h2 class="menudivider">University Bookstore</h2>
    <ul>
		<li class="graduation"><a href="http://www.fortyninershops.net/site_graduation.asp?">Graduation Center</a></li>
        <li class="faculty"><a href="http://www.fortyninershops.net/site_faculty.asp?">Faculty Resource Center</a></li>
        <li class="copy"><a href="http://www.fortyninershops.net/site_campus_copy_center.asp?">Campus Copy Center</a></li>
  		<li class="returns"><a href="http://www.fortyninershops.net/site_return_guidelines.asp?">Return Guidelines</a></li>
  		<li class="rentguide"><a href="http://www.fortyninershops.net/site_rental_guidelines.asp?">Rental Guidelines</a></li>
  		<li class="shipping"><a href="http://www.fortyninershops.net/site_shipping_info_ut.asp?">Shipping Information</a></li>  
        <li class="promos"><a href="http://www.fortyninershops.net/site_promotions_events.asp?">Promotions &amp; Events</a></li>
    	<li class="hours"><a href="http://www.fortyninershops.net/site_hours.asp?">Hours of Operation</a></li>
    	<li class="contact"><a href="http://www.fortyninershops.net/site_contact_us.asp?">Contact Us</a></li>
	</ul>
    </div>
    <a name="content" id="content"></a>
    <div id="contain_column2">
		<div id="main">
<script type="text/javascript" src="/innerweb/v3.0/include/js/shop_product_detail_modal.js"></script>



<input type="hidden" id="display_product_zoom" value="0" />
<a name="content"></a>
<h1><span>Shorts/pants</span></h1><div class="breadcrumbs-cont"><p>You are here:</p><ul class="breadcrumbs"><li><a href="http://www.fortyninershops.net/shop_main.asp?">Home</a> �</li>
<li><a href="http://www.fortyninershops.net/shop_main.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc">Clothing</a> �</li>
<li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=681&catalog_name=U2hvcnRzL1BhbnRz">Shorts/Pants</a> �</li>
<li>Product Detail</li></ul></div><div id="product-detail">
<div id="product-info">
<h2 class="product-name">Csulb Inside Csulb Pant Blk Russell</h2>
<dl id="product-price-info"><dt class="product-price">Our Price:</dt><dd class="product-price">$29.95</dd></dl>
<script type="text/javascript">
btn0_0 = new Button('BLACK',"");
agColor = new AttributeGroup("Color",btn0_0);

btn1_0 = new Button('L',"");
btn1_1 = new Button('M',"");
btn1_2 = new Button('XL',"");
btn1_3 = new Button('XXL',"");
agSize = new AttributeGroup("Size",btn1_0,btn1_1,btn1_2,btn1_3);

itemList={"items": [
{"available":true,"itemNumber":"12261550290","price":"$29.95","availability": "Ships in 2 Business Days","buttons":[btn0_0,btn1_1]},
{"available":true,"itemNumber":"12261567290","price":"$29.95","availability": "Ships in 2 Business Days","buttons":[btn0_0,btn1_0]},
{"available":true,"itemNumber":"12261574290","price":"$29.95","availability": "Ships in 2 Business Days","buttons":[btn0_0,btn1_2]},
{"available":true,"itemNumber":"12261581290","price":"$29.95","availability": "Ships in 2 Business Days","buttons":[btn0_0,btn1_3]}
]
}
psObj = new ProductSelector(itemList ,agColor,agSize);
createProductSelector = function(){
psObj.setItemNumberField("frmCart", "pf_sku");
psObj.buildProductSelector("product-options-list");
}
addEvent(window,'load',createProductSelector);
</script><div id="na"></div><div id="product-options-list"></div>
<form action="http://www.fortyninershops.net/xt_orderform_additem.asp?" method="post" name="frmCart" id="frmCart" onsubmit="validateAddToCart(); return false;">
<div id="product-summary">
<table><tr><td colspan="3">
<div id="product-availability">

</div>
</td></tr>
<tr><td id="qty">qty <input type="text" name="qty" id="fQty" class="box" value="1" maxlength="3"/></td><td id="options"><em>Please make a selection above</em></td><td id="price"></td></tr>
</table></div>
<input type="hidden" name="pf_id" id="pf_id" value="3940"/>
<input type="hidden" name="target" value="shop_product_list.asp"/>
<input type="hidden" name="catalog_group_name" value="Q2xvdGhpbmc"/>
<input type="hidden" name="catalog_group_id" value="MzY"/>
<input type="hidden" name="catalog_id" value="681"/>
<input type="hidden" name="catalog_name" value="U2hvcnRzL1BhbnRz"/>

<input type="hidden" name="type" value="1"/>
<input type="hidden" name="sku_id" id="pf_sku"/><input type="submit" name="addcart" value="+ add to cart" class="button" disabled="true"/>

</form>
</div>
<div id="product-attribute-photo"></div><div id="product-photo"><a href="http://www.fortyninershops.net/outerweb/product_images/12261543l.jpg" target="productphoto" class="product-photo" onclick="enlarge('http://www.fortyninershops.net/outerweb/product_images/12261543l.jpg');return false;" rel="lightbox" title="Csulb Inside Csulb Pant Blk Russell"><img src="http://www.fortyninershops.net/outerweb/product_images/12261543l.jpg" alt="Csulb Inside Csulb Pant Blk Russell (SKU 12261550290)" title="Csulb Inside Csulb Pant Blk Russell (SKU 12261550290)" width="170" /></a> <a id="enlarge" href="http://www.fortyninershops.net/outerweb/product_images/12261543l.jpg" onclick="enlarge('http://www.fortyninershops.net/outerweb/product_images/12261543l.jpg'); return false;" rel="lightbox" title="Csulb Inside Csulb Pant Blk Russell">enlarge image</a></div></div>
<div id="product-desc" class="product-block"><h3><span>Product Description</span></h3>
<p>50% Cotton 50% Polyester. Black full length sweats with gold and white printed lettering along the left leg. Reads CSULB and in the middle going through it reads California State University Long Beach.</p></div><div id="tellafriend" class="product-block"><p>Tell a Friend &raquo; <a href="#" onclick="showEmailForm(this); return false;">Email</a>/<a href="#" onclick="showSMSForm(this); return false;">Text Message</a></p><div class="tellafriend-email tellafriend-method hide"><h4><span>Email To A Friend</span></h4><form action="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=681&catalog_name=U2hvcnRzL1BhbnRz&pf_id=3940&product_name=Q3N1bGIgSW5zaWRlIENzdWxiIFBhbnQgQmxrIFJ1c3NlbGw&type=1&target=shop_product_list.asp" method="post" id="form-tella-email" name="form-tella-email"><input type="hidden" name="taf_id" value="3940"/><input type="hidden" name="pageaction" value="email"/><p><label for="email-sendname">Your Name</label><br /><input type="text" name="txtName" id="email-sendname" class="box customsize" size="25" /></p><p><label for="email-sendemail">Your Email Address</label><br /><input type="text" name="txtSendEmail" id="email-sendemail" class="box customsize" size="25" /></p><p><label for="email-toemail">Recipient's Email Address</label><br /><input type="text" name="txtToEmail" id="email-toemail" class="box customsize" size="25" /></p><input type="submit" value="send" class="button" >&nbsp;<input type="button" value="cancel" class="button secondary" onclick="cancel(this);" /></form></div><div class="tellafriend-sms tellafriend-method hide"><h4><span>Text To A Friend</span></h4><form action="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=681&catalog_name=U2hvcnRzL1BhbnRz&pf_id=3940&product_name=Q3N1bGIgSW5zaWRlIENzdWxiIFBhbnQgQmxrIFJ1c3NlbGw&type=1&target=shop_product_list.asp" method="post" id="form-tella-text" name="form-tella-text"><input type="hidden" name="taf_id" value="3940"/><input type="hidden" name="pageaction" value="textmessage"/><p><label for="text-sendname">Your Name</label><br /><input type="text" name="txtName" id="text-sendname" class="box customsize" maxlength="25" size="25" /></p><p><label for="text-to">Recipient's Cell Number</label><br /><input type="text" name="txtToEmail" id="text-to" class="box customsize" size="25" /></p><input type="submit" value="send" class="button" />&nbsp;<input type="button" value="cancel" class="button secondary" onclick="cancel(this);" / ></form></div></div><input value="98BF5D6B5478429099D8EB64BA688AF4" name="rating-shopper" type="hidden" id="rating-shopper" /><input value="https://www.fortyninershops.net/account_login.asp?formItems=35648&amp;target=su.asp" name="login_url" type="hidden" id="login_url" /><div class="product-block" id="product_rating_block"><div id="product_rating_label"><h3>Rate this &raquo;</h3></div><div class="product_rating" id="product_rating-0"><a href="#" class="rating_seg rating_seg_l rating_seg_i" id="rating-0-0"></a><a href="#" class="rating_seg rating_seg_r rating_seg_i" id="rating-1-0"></a><a href="#" class="rating_seg rating_seg_l rating_seg_i" id="rating-2-0"></a><a href="#" class="rating_seg rating_seg_r rating_seg_i" id="rating-3-0"></a><a href="#" class="rating_seg rating_seg_l rating_seg_i" id="rating-4-0"></a><a href="#" class="rating_seg rating_seg_r rating_seg_i" id="rating-5-0"></a><a href="#" class="rating_seg rating_seg_l rating_seg_i" id="rating-6-0"></a><a href="#" class="rating_seg rating_seg_r rating_seg_i" id="rating-7-0"></a><a href="#" class="rating_seg rating_seg_l rating_seg_i" id="rating-8-0"></a><a href="#" class="rating_seg rating_seg_r rating_seg_i" id="rating-9-0"></a><input value="-1" name="prod_rating-0" type="hidden" id="prod_rating-0" /><input value="-2" name="user_rating-0" type="hidden" id="user_rating-0" /><input value="3940" name="pf_id-0" type="hidden" id="pf_id-0" /></div><span id="rating_message-0" class="rating_message"></span></div>
</div>
    </div>
    <div id="contain_column3"></div>
<div class="clear_float"></div>
</div>
<div class="minheight"></div>
</div>
<div id="footer">
    <div id="contain_footer">
        <div id="contain_address">
            <div id="address_content">
                <address>
                <strong>Forty-Niner Shops Inc.</strong><br />
                    6049 East 7th Street<br />
                    Long Beach, CA 90840<br />
                    562.985.5093
                </address>
            </div>
        </div>
        <div id="contain_utilities">
            <ul>
              <li><a href="http://www.csulb.edu/aux/49ershops/corporate/privacy/" title="Terms of Use" target="_blank">Terms of Use</a></li>
            </ul>
        </div>
        <div class="clear_float"></div>
    </div>
</div>


</body>

</html>
