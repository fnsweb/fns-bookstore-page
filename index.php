<html xmlns="http://www.w3.org/1999/xhtml"><head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>Welcome | 49er Shops Bookstore</title>

<link rel="stylesheet" href="custom.css">
<link rel="stylesheet" href="clearfix.css">
<link rel="stylesheet" href="css/over.css">

<style type="text/css" media="all">
  @import url("http://staging.thecampushub.com/innerweb/v3.0/styles/campushub.css");
  @import url("http://staging.thecampushub.com/innerweb/v3.0/styles/delta/delta.css");
</style>
<script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script><script type="text/javascript" src="http://staging.thecampushub.com/innerweb/v3.0/include/js/campushub.js"></script>
<script type="text/javascript" src="http://staging.thecampushub.com/innerweb/v3.0/include/js/jquery.js"></script>
<script type="text/javascript" src="http://staging.thecampushub.com/innerweb/v3.0/include/js/modals.js"></script>
<script type="text/javascript" src="http://staging.thecampushub.com/innerweb/v3.0/include/js/jqmodal.js"></script>
<script type="text/javascript" src="http://staging.thecampushub.com/innerweb/v3.0/include/js/set_modal_div_and_cart_summary_values.js"></script>

<script type="text/javascript" src="http://staging.thecampushub.com/fortyniner/scripts/dt-styleswitcher.js"></script><script type="text/javascript" src="http://staging.thecampushub.com/fortyniner/scripts/jquery-1.4.2.min.js"></script><script type="text/javascript" src="http://staging.thecampushub.com/fortyniner/scripts/jquery-ui-1.8.1.custom.min.js"></script><script type="text/javascript" src="http://staging.thecampushub.com/fortyniner/scripts/rotator.js"></script><script type="text/javascript" src="http://staging.thecampushub.com/fortyniner/scripts/ut_menu.js"></script>
<script type="text/javascript">


 function doLoad(){
  setFocusOnError();enableSimpleFormValidation();autoLinkInit();
 }
 window.onload = doLoad
</script>

<!-- Begin University Template stylesheets -->
<link href="http://staging.thecampushub.com/fortyniner/stylesheets/ut-print.css" rel="stylesheet" type="text/css" media="print">
<link href="http://staging.thecampushub.com/fortyniner/stylesheets/ut-medium.css" rel="stylesheet" type="text/css">
<link rel="alternate stylesheet" type="text/css" media="screen,print" href="http://staging.thecampushub.com/fortyniner/stylesheets/ut-small.css" title="small_text">
<link rel="alternate stylesheet" type="text/css" media="screen,print" href="http://staging.thecampushub.com/fortyniner/stylesheets/ut-medium.css" title="medium_text">
<link rel="alternate stylesheet" type="text/css" media="screen,print" href="http://staging.thecampushub.com/fortyniner/stylesheets/ut-large.css" title="large_text">
<link href="http://staging.thecampushub.com/fortyniner/stylesheets/ut.css" rel="stylesheet" type="text/css" media="screen">
<link href="http://staging.thecampushub.com/fortyniner/stylesheets/website.css" rel="stylesheet" type="text/css">

<link href="http://staging.thecampushub.com/fortyniner/stylesheets/home.css" media="screen" rel="stylesheet" type="text/css">

<link href="http://staging.thecampushub.com/fortyniner/assets/images/favicon.ico" rel="shortcut icon">
<!-- End University Template stylesheets -->

<link type="text/css" href="http://staging.thecampushub.com/fortyniner/stylesheets/rotator.css" rel="stylesheet">

<style type="text/css" media="all">
@import url("http://staging.thecampushub.com/fortyniner/stylesheets/customize.css");
</style>
<!-- LocalRef:18 -->

<script type="text/javascript" src="http://staging.thecampushub.com/Innerweb/v3.0/include/js/cookies.js"></script>
<script type="text/javascript">
<!--
var referrer;
var rootURL = 'http://staging.thecampushub.com/fortyniner/';
var rootSURL = 'https://staging.thecampushub.com/fortyniner/';

if (cookies_enabled) {

referrer = getCookie('referring_url');
if (!referrer) {
    referrer = '';
if (referrer == ''){
referrer = 'None';
}else if((referrer.indexOf(rootURL) >= 0) || (referrer.indexOf(rootSURL) >= 0)){
referrer = 'Local';
}
setCookie('referring_url', referrer);
referrer = getCookie('referring_url');
if(!referrer){
referrer = 'Unknown';
}
}else{
    var referring_page = '';
if ((referring_page.indexOf(rootURL) >= 0) ||(referring_page.indexOf(rootSURL) >= 0) ||(referring_page == '')) {
} else {
referrer = referring_page
setCookie('referring_url', referrer);
}
}
} else {
referrer = 'Unknown';
}
//-->
</script>



<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7347222-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">
function validateForm(frm) {

       var formIsValid = true;



       // find all elements that contain the class "validate-*"

      $("*[class*='validate-'][disabled='']",frm).each(function() {

            formIsValid = (validateField(this)) ? formIsValid : false;

        });

        

        $("*[class*='validateconditional-'][disabled='']",frm).each(function() {

            formIsValid = (validateConditionalField(this)) ? formIsValid : false;

        });   

        

    

       

       return formIsValid;

   }

</script>
</head>

<body class="home" id="home">

  <div class="clear_float"><a href="index.html#localnav">Skip to Local Navigation</a></div>
<div class="clear_float"><a href="index.html#content">Skip to Content</a></div>
<div id="contain_header">
  <div id="contain_header_setwidth">
    <div id="contain_siteid">
      <div id="siteid_content">
        <a href="http://www.csulb.edu/" target="_blank"><img src="http://staging.thecampushub.com/fortyniner/assets/UT/hdr_blk_id.gif" alt="California State University, Long Beach" width="287" height="50" border="0" title="California State University, Long Beach"></a>
       </div>
    </div>
    <div id="contain_search">
      <div id="content_searchtools">
      <ul id="searchtools_links">
    <li><div id="gSearchbox">
    <form action="http://staging.thecampushub.com/fortyniner/search_advanced_results.asp?" method="post" name="siteSearch" id="frm-sitesearch">
        <label for="txtSearch"><span>search</span><img src="http://staging.thecampushub.com/innerweb/v3.0/styles/delta/images/search_icon.png" alt="search" height="15" width="15"></label>
        <input name="search_text" id="txtSearch" type="text" class="box" maxlength="50" value="">&nbsp;
        <select name="search_scope" id="search_scope" class="box">
            <option value="All">All Products</option>
            <option value="Textbooks">Textbooks</option>
            <option value="Merchandise">Merchandise</option>
            <option value="General Books">General Books</option>
        </select>&nbsp;
        <input type="hidden" name="pageaction" value="redirect">
        <!--<input type="submit" value="GO" class="button" />-->
    <input type="image" value="GO" class="sitesearchbutton" src="http://staging.thecampushub.com/fortyniner/assets/interface/searchbutton_icon.gif" alt="Search">
    </form>
</div>
</li>
    </ul>
    </div>
  </div>
  <div class="clear_float"></div>
</div>
</div>
<div id="contain_divisionnav">
    <div id="divisionnav_content">
      <ul id="division_links">
      <li class="phome"><a href="http://www.csulb.edu/aux/49ershops/corporate/">Forty-Niner Shops</a></li>
        <li class="phome"><a href="http://staging.thecampushub.com/fortyniner/default.asp?">Bookstore Home</a></li>
            
            <!-- Changed for clear serparation of textbook options 2013.06.17, david -->
            <span style="color:#fff;display:inline-block; margin: 0 10px 0 7px;">|</span>
            <li class="phome" style="color:#fff;font-weight:bold;margin-right:5px;">Textbooks:</li>
            <!-- disabled WebPRISM Buy link in favor of VerbaCompare  2011.07.17, ali
            <li class="pbuy"><a href="http://staging.thecampushub.com/fortyniner/buy_main.asp?" title="Buy Textbooks">Buy</a></li> -->
            
            <li class="pbuy"><a href="http://csulb.verbacompare.com/">Buy/Rent</a></li>
            
            
                <li class="psell"><a href="http://staging.thecampushub.com/fortyniner/sell_main.asp?" title="Textbook Buyback: Sell Your Textbooks">Sell</a></li>
            
            
            <!-- Disabled by Ali on 12.12.2010 Per Fred and Rosa's Request
            < if BuybackClassifiedsEnabled=1 then %>
                <li class="pswap"><a href="<= baseURL("swap_main.asp")%>" title="Swap your textbooks with other students">Swap</a></li>
            < end if %>

            -->
            
            <!-- Disabled Chegg in favor of VerbaCompare 7/18/2011, ali
            <li class="phome"><a href="http://www.shopthebeach.com/go/chegg" title="Rent Textbooks & Save Hundreds" target="_blank">Rent</a></li> -->
            
            <li class="phome"><a href="http://staging.thecampushub.com/fortyniner/site_faculty.asp?" title="Faculty Resource Center"> Faculty</a></li>

      </ul>
  </div>
  </div> 
  <div id="contain_banner">
      <a href="http://staging.thecampushub.com/fortyniner/default.asp?">
      <img src="http://staging.thecampushub.com/fortyniner/assets/banners/banner_your_campus_your_bookstore_20100721.jpg" alt="49er Shops Bookstore" title="49er Shops Bookstore" width="900" height="67" border="0">
    </a>
  </div>  

<!-- Edited with textwrangler  -->
<a href="index.html#content" title="skip navigation" class="accessibility">skip to main content</a><div id="contain_toolset">
<div id="contain_toolbar">
  <!-- ***** Breadcrumbs container -->
  <div id="contain_breadcrumbs">
    <div id="breadcrumbs_content">
    <!-- BreadCrumbs usage: BreadCrumbs("homePage","sepChars","linkHome",UToSpace,DToSpace,changeCaps,hideExt); -->
        <!-- homePage = "HOME";     // text name for home page link
     sepChars = " &gt; ";   // character(s) to sepCharsarate links
     linkHome = "/";      // base URL for links (for subfolders)
     UToSpace = true;     // change all underscores to spaces in folder names
     DToSpace = true;     // change all dashes to spaces in folder names
     changeCaps = 0;      // 0 = no change, 1 = Initial Caps, 2 = All Upper, 3 = All Lower
     hideExt = true;      // hide extenion in file name -->
        <!--<script type="text/javascript">BreadCrumbs("Home"," &#8250; ","",true,true,1,true);</script>
        <noscript>
          Please enable JavaScript in your browser.
        </noscript>-->
        <!-- Social Networking Icons -->
        <!--<img src="http://staging.thecampushub.com/fortyniner/assets/interface/vert_divider_2px.gif" alt="Vertical Divider" width="2" height="17" />-->
        <a href="http://www.facebook.com/CSULBbookstore" title="Like Us on Facebook" target="_blank"><img src="http://staging.thecampushub.com/fortyniner/assets/icons/social/facebook_icon_17x17.png" alt="Like Us on Facebook" width="17" height="17"></a>
        <a href="http://twitter.com/csulbbookstore" title="Follow Us on Twitter" target="_blank"><img src="http://staging.thecampushub.com/fortyniner/assets/icons/social/twitter_icon_17x17.png" alt="Follow Us on Twitter" width="17" height="17"></a>
        <!-- End Social Networking Icons -->
  </div>
  </div>
  <!-- Container for Tools -->
  <div id="contain_tools">
    <!-- Container for Tools Content -->
    <div id="tools_content">
        <a href="index.html#" onclick="window.print();"><img src="http://staging.thecampushub.com/fortyniner/assets/icons/tool_print.gif" alt="Print this page" width="21" height="17"></a>
        <a href="index.html#" onclick="addToFavorites();"><img src="http://staging.thecampushub.com/fortyniner/assets/icons/tool_favorite.gif" alt="Add this page to your favorites" width="21" height="17"></a>
        <!-- Text Size Controls -->
        <img src="http://staging.thecampushub.com/fortyniner/assets/icons/tool_fontsizelabel.gif" alt="Select a font size" width="58" height="17">
        <a href="index.html#" onclick="setActiveStyleSheet('small_text', 1);"><img src="http://staging.thecampushub.com/fortyniner/assets/icons/tool_small.gif" alt="Select a small font" width="17" height="17"></a>
        <a href="index.html#" onclick="setActiveStyleSheet('medium_text', 1);"><img src="http://staging.thecampushub.com/fortyniner/assets/icons/tool_medium.gif" alt="Select a medium font" width="17" height="17"></a>
        <a href="index.html#" onclick="setActiveStyleSheet('large_text', 1);"><img src="http://staging.thecampushub.com/fortyniner/assets/icons/tool_large.gif" alt="Select a large font" width="17" height="17"></a>
        <!-- End Text Size Controls -->
  </div>
  <!-- ***** Breadcrumbs_home area ends here ***** -->
  </div>
  <!-- ***** End Printable ***** -->
  <!-- Clear floats so that standards compliant browsers display the background color -->
  <div class="clear_float">&nbsp;</div>
</div>
</div>
    
    <div id="container">
        <div id="contain_content">
          <a name="localnav" id="localnav"></a>
      
<div id="contain_column1">
      <ul>
  <li class="home"><a href="http://staging.thecampushub.com/fortyniner/default.asp?">Home</a></li>
</ul>
  <h2 class="menudivider">Account Info</h2>
  <div id="user-summary">logged in as David Nuon<br><a href="https://staging.thecampushub.com/fortyniner/account_main.asp?">my account</a> | <a href="http://staging.thecampushub.com/fortyniner/account_login.asp?logout=true">logout</a></div>
  <div class="clear_float"></div>
          <div id="cart-summary">       <strong>My Cart</strong><br><span id="cartSummaryValues">1 item |$39.95</span><br><span id="cartSummaryLink"><strong><a href="http://staging.thecampushub.com/fortyniner/cart.asp?">View Cart »</a></strong></span><br></div>

  <div class="clear_float"></div>
  <ul>
        <li class="orders"><a href="http://staging.thecampushub.com/fortyniner/account_orderhistory.asp?">Order History</a></li> 
  </ul>
      <ul>
  <li class="home"><a href="http://staging.thecampushub.com/fortyniner/default.asp?">Home</a></li>
</ul>
  <h2 class="menudivider">Account Info</h2>
  <div id="user-summary">you are not logged in<br><a href="https://staging.thecampushub.com/fortyniner/account_login.asp?">login</a></div>
  <div class="clear_float"></div>
          <div id="cart-summary">       <strong>My Cart</strong><br><span id="cartSummaryValues">0 items</span><br><span id="cartSummaryLink"></span><br></div>

  <div class="clear_float"></div>
  <ul>
        <li class="orders"><a href="http://staging.thecampushub.com/fortyniner/account_orderhistory.asp?">Order History</a></li> 
  </ul><h2 class="menudivider">Textbooks</h2>
    <ul>
        <!-- disabled WebPRISM Buy page in favor of VerbeCompare, 2011.07.18, ali
        <li class="buy"><a href="http://staging.thecampushub.com/fortyniner/buy_main.asp?" title="Buy Textbooks">Buy/Rent</a></li> -->
        
        <li class="buy"><a href="http://csulb.verbacompare.com">Buy/Rent</a></li>
        
        <!--Rent Link Disabled
        <li class="phome"><a href="http://www.shopthebeach.com/go/chegg" title="Rent Textbooks & Save Hundreds" target="_blank">Rent</a></li>
        -->
        
        
    <li class="sell"><a href="http://staging.thecampushub.com/fortyniner/sell_main.asp?">Sell</a></li>
        
    <li class="trade"><a href="http://staging.thecampushub.com/fortyniner/swap_main.asp?">Trade</a></li>
    

        <li><a href="http://staging.thecampushub.com/fortyniner/site_textbook_options_dare_to_compare.asp?">Textbook Options</a></li>   

        <li><a href="http://staging.thecampushub.com/fortyniner/site_csu_digital_rental.asp?">Digital Rentals</a></li>       
    </ul>

<h2 class="menudivider">Shop The Beach</h2>
  
<div id="sub-nav">
<h2>Browse</h2><ul><li><a href="http://staging.thecampushub.com/fortyniner/shop_main.asp?catalog_group_id=MzY&amp;catalog_group_name=Q2xvdGhpbmc">Clothing</a></li><li><a href="http://staging.thecampushub.com/fortyniner/shop_main.asp?catalog_group_id=Mzc&amp;catalog_group_name=R2lmdHM">Gifts</a></li><li><a href="http://staging.thecampushub.com/fortyniner/shop_main.asp?catalog_group_id=Mzk&amp;catalog_group_name=VGVjaG5vbG9neQ">Technology</a></li><li><a href="http://staging.thecampushub.com/fortyniner/shop_main.asp?catalog_group_id=NDE&amp;catalog_group_name=Q3N1bGIgUHVibGljYXRpb25z">Csulb Publications</a></li><li><a href="http://staging.thecampushub.com/fortyniner/shop_main.asp?catalog_group_id=NDM&amp;catalog_group_name=U2Nob29sIEVzc2VudGlhbHM">School Essentials</a></li></ul>
</div><h2 class="menudivider">University Bookstore</h2>
    <ul>
    <li class="graduation"><a href="http://staging.thecampushub.com/fortyniner/site_graduation.asp?">Graduation Center</a></li>
        <li class="faculty"><a href="http://staging.thecampushub.com/fortyniner/site_faculty.asp?">Faculty Resource Center</a></li>
        <li class="copy"><a href="http://staging.thecampushub.com/fortyniner/site_campus_copy_center.asp?">Campus Copy Center</a></li>
      <li class="returns"><a href="http://staging.thecampushub.com/fortyniner/site_return_guidelines.asp?">Return Guidelines</a></li>
      <li class="rentguide"><a href="http://staging.thecampushub.com/fortyniner/site_rental_guidelines.asp?">Rental Guidelines</a></li>
      <li class="shipping"><a href="http://staging.thecampushub.com/fortyniner/site_shipping_info_ut.asp?">Shipping Information</a></li>  
        <li class="promos"><a href="http://staging.thecampushub.com/fortyniner/site_promotions_events.asp?">Promotions &amp; Events</a></li>
      <li class="hours"><a href="http://staging.thecampushub.com/fortyniner/site_hours.asp?">Hours of Operation</a></li>
      <li class="contact"><a href="http://staging.thecampushub.com/fortyniner/site_contact_us.asp?">Contact Us</a></li>
  </ul>
    </div>
    <a name="content" id="content"></a>
     
<?php include('front_page_column.php'); ?>
    
<div class="clear_float"></div>
</div>
<div class="minheight"></div>
</div>
<div id="footer">
    <div id="contain_footer">
        <div id="contain_address">
            <div id="address_content">
                <address>
                <strong>Forty-Niner Shops Inc.</strong><br>
                    6049 East 7th Street<br>
                    Long Beach, CA 90840<br>
                    562.985.5093
                </address>
            </div>
        </div>
        <div id="contain_utilities">
            <ul>
              <li><a href="http://www.csulb.edu/aux/49ershops/corporate/privacy/" title="Terms of Use" target="_blank">Terms of Use</a></li>
            </ul>
        </div>
        <div class="clear_float"></div>
    </div>
</div>




<div id="toolTip" style="top: 0px; visibility: hidden;"></div></body></html>
