<?php include("doctop.php"); ?>
<body  id=shop>

	<div class="clear_float"><a href="#localnav">Skip to Local Navigation</a></div>
<div class="clear_float"><a href="#content">Skip to Content</a></div>
<div id="contain_header">
  <div id="contain_header_setwidth">
    <div id="contain_siteid">
      <div id="siteid_content">
      	<a href="http://www.csulb.edu/" target="_blank"><img src="http://www.fortyninershops.net//assets/UT/hdr_blk_id.gif" alt="California State University, Long Beach" width="287" height="50" border="0" title="California State University, Long Beach" /></a>
       </div>
    </div>
    <div id="contain_search">
      <div id="content_searchtools">
      <ul id="searchtools_links">
	  <li><div id="gSearchbox">
    <form action="http://www.fortyninershops.net/search_advanced_results.asp?" method="post" name="siteSearch" id="frm-sitesearch">
        <label for="txtSearch"><span>search</span><img src="http://www.fortyninershops.net/v3.0/styles/delta/images/search_icon.png" alt="search" height="15" width="15" /></label>
        <input name="search_text" id="txtSearch" type="text" class="box" maxlength="50" value=""/>&nbsp;
        <select name="search_scope" id="search_scope" class="box">
            <option value="All">All Products</option>
            <option value="Textbooks">Textbooks</option>
            <option value="Merchandise">Merchandise</option>
            <option value="General Books">General Books</option>
        </select>&nbsp;
        <input type="hidden" name="pageaction" value="redirect"/>
        <!--<input type="submit" value="GO" class="button" />-->
		<input type="image" value="GO" class="sitesearchbutton" src="http://www.fortyninershops.net//assets/interface/searchbutton_icon.gif" alt="Search" />
    </form>
</div>
</li>
    </ul>
    </div>
  </div>
  <div class="clear_float"></div>
</div>
</div>
<div id="contain_divisionnav">
  	<div id="divisionnav_content">
    	<ul id="division_links">
			<li class="phome"><a href="http://www.csulb.edu/aux/49ershops/corporate/">Forty-Niner Shops</a></li>
     		<li class="phome"><a href="http://www.fortyninershops.net/default.asp?">Bookstore Home</a></li>
            
            <!-- Changed for clear serparation of textbook options 2013.06.17, david -->
            <span style="color:#fff;display:inline-block; margin: 0 10px 0 7px;">|</span>
            <li class="phome" style="color:#fff;font-weight:bold;margin-right:5px;">Textbooks:</li>
            <!-- disabled WebPRISM Buy link in favor of VerbaCompare  2011.07.17, ali
            <li class="pbuy"><a href="http://www.fortyninershops.net/buy_main.asp?" title="Buy Textbooks">Buy</a></li> -->
            
            <li class="pbuy"><a href="http://csulb.verbacompare.com/">Buy/Rent</a></li>
            
            
                <li class="psell"><a href="http://www.fortyninershops.net/sell_main.asp?" title="Textbook Buyback: Sell Your Textbooks">Sell</a></li>
            
            
            <!-- Disabled by Ali on 12.12.2010 Per Fred and Rosa's Request
            < if BuybackClassifiedsEnabled=1 then %>
                <li class="pswap"><a href="<= baseURL("swap_main.asp")%>" title="Swap your textbooks with other students">Swap</a></li>
            < end if %>

            -->
            
            <!-- Disabled Chegg in favor of VerbaCompare 7/18/2011, ali
            <li class="phome"><a href="http://www.shopthebeach.com/go/chegg" title="Rent Textbooks & Save Hundreds" target="_blank">Rent</a></li> -->
            
            <li class="phome"><a href="http://www.fortyninershops.net/site_faculty.asp?" title="Faculty Resource Center"> Faculty</a></li>

    	</ul>
	</div>
  </div> 
	<div id="contain_banner_secondary">
  		<a href="http://www.fortyninershops.net/default.asp?">
  			<img src="http://www.fortyninershops.net//assets/banners/banner_university_bookstore.jpg" alt="49er Shops Bookstore" title="49er Shops Bookstore" width="900" height="67" border="0" />
  		</a>
	</div>

<!-- Edited with textwrangler  -->
<a href="#content" title="skip navigation" class="accessibility">skip to main content</a><div id="contain_toolset">
<div id="contain_toolbar">
  <!-- ***** Breadcrumbs container -->
  <div id="contain_breadcrumbs">
    <div id="breadcrumbs_content">
		<!-- BreadCrumbs usage: BreadCrumbs("homePage","sepChars","linkHome",UToSpace,DToSpace,changeCaps,hideExt); -->
        <!-- homePage = "HOME";			// text name for home page link
		 sepChars = " &gt; ";		// character(s) to sepCharsarate links
		 linkHome = "/";			// base URL for links (for subfolders)
		 UToSpace = true;			// change all underscores to spaces in folder names
		 DToSpace = true;			// change all dashes to spaces in folder names
		 changeCaps = 0;			// 0 = no change, 1 = Initial Caps, 2 = All Upper, 3 = All Lower
		 hideExt = true;			// hide extenion in file name -->
        <!--<script type="text/javascript">BreadCrumbs("Home"," &#8250; ","",true,true,1,true);</script>
        <noscript>
        	Please enable JavaScript in your browser.
        </noscript>-->
        <!-- Social Networking Icons -->
        <!--<img src="http://www.fortyninershops.net//assets/interface/vert_divider_2px.gif" alt="Vertical Divider" width="2" height="17" />-->
        <a href="http://www.facebook.com/CSULBbookstore" title="Like Us on Facebook" target="_blank"><img src="http://www.fortyninershops.net//assets/icons/social/facebook_icon_17x17.png" alt="Like Us on Facebook" width="17" height="17" /></a>
        <a href="http://twitter.com/csulbbookstore" title="Follow Us on Twitter" target="_blank"><img src="http://www.fortyninershops.net//assets/icons/social/twitter_icon_17x17.png" alt="Follow Us on Twitter" width="17" height="17" /></a>
        <!-- End Social Networking Icons -->
	</div>
  </div>
  <!-- Container for Tools -->
  <div id="contain_tools">
    <!-- Container for Tools Content -->
    <div id="tools_content">
        <a href="#" onclick="window.print();"><img src="http://www.fortyninershops.net//assets/icons/tool_print.gif" alt="Print this page" width="21" height="17" /></a>
        <a href="#" onclick="addToFavorites();"><img src="http://www.fortyninershops.net//assets/icons/tool_favorite.gif" alt="Add this page to your favorites" width="21" height="17" /></a>
        <!-- Text Size Controls -->
        <img src="http://www.fortyninershops.net//assets/icons/tool_fontsizelabel.gif" alt="Select a font size" width="58" height="17" />
        <a href="#" onclick="setActiveStyleSheet('small_text', 1);"><img src="http://www.fortyninershops.net//assets/icons/tool_small.gif" alt="Select a small font" width="17" height="17" /></a>
        <a href="#" onclick="setActiveStyleSheet('medium_text', 1);"><img src="http://www.fortyninershops.net//assets/icons/tool_medium.gif" alt="Select a medium font" width="17" height="17" /></a>
        <a href="#" onclick="setActiveStyleSheet('large_text', 1);"><img src="http://www.fortyninershops.net//assets/icons/tool_large.gif" alt="Select a large font" width="17" height="17" /></a>
        <!-- End Text Size Controls -->
	</div>
	<!-- ***** Breadcrumbs_home area ends here ***** -->
  </div>
  <!-- ***** End Printable ***** -->
  <!-- Clear floats so that standards compliant browsers display the background color -->
  <div class="clear_float">&nbsp;</div>
</div>
</div>
    
    <div id="container">
        <div id="contain_content">
          <a name="localnav" id="localnav"></a>
      
    <div id="contain_column1">
		<ul>
  <li class="home"><a href="http://www.fortyninershops.net/default.asp?">Home</a></li>
</ul>
	<h2 class="menudivider">Account Info</h2>
	<div id="user-summary">you are not logged in<br /><a href="https://www.fortyninershops.net/account_login.asp?">login</a></div>
	<div class="clear_float"></div>
    			<div id="cart-summary">				<strong>My Cart</strong><br /><span id="cartSummaryValues">0 items</span><br/><span id="cartSummaryLink"></span><br/></div>

	<div class="clear_float"></div>
	<ul>
      	<li class="orders"><a href="http://www.fortyninershops.net/account_orderhistory.asp?">Order History</a></li> 
	</ul><h2 class="menudivider">Textbooks</h2>
    <ul>
        <!-- disabled WebPRISM Buy page in favor of VerbeCompare, 2011.07.18, ali
        <li class="buy"><a href="http://www.fortyninershops.net/buy_main.asp?" title="Buy Textbooks">Buy/Rent</a></li> -->
        
        <li class="buy"><a href="http://csulb.verbacompare.com">Buy/Rent</a></li>
        
        <!--Rent Link Disabled
        <li class="phome"><a href="http://www.shopthebeach.com/go/chegg" title="Rent Textbooks & Save Hundreds" target="_blank">Rent</a></li>
        -->
        
        
		<li class="sell"><a href="http://www.fortyninershops.net/sell_main.asp?">Sell</a></li>
        
		<li class="trade"><a href="http://www.fortyninershops.net/swap_main.asp?">Trade</a></li>
		

        <li><a href="http://www.fortyninershops.net/site_textbook_options_dare_to_compare.asp?">Textbook Options</a></li>   

        <li><a href="http://www.fortyninershops.net/site_csu_digital_rental.asp?">Digital Rentals</a></li>       
    </ul>

<h2 class="menudivider">Shop The Beach</h2>
	
<div id="sub-nav">
<h2>Browse</h2><h3>Clothing</h3><ul><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=701&catalog_name=Mk5kIFN0cmVldCBTcG90bGlnaHQ">2Nd Street Spotlight</a></li><li class="selected"><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz">9.49 Tees</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=655&catalog_name=QWx1bW5p">Alumni</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=648&catalog_name=QmVhY2ggR2lybA">Beach Girl</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=683&catalog_name=RGlydGJhZ3M">Dirtbags</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=654&catalog_name=RmFtaWx5">Family</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=670&catalog_name=Rm9vdHdlYXI">Footwear</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=646&catalog_name=SGVhZHdlYXI">Headwear</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=678&catalog_name=SmVyc2V5cw">Jerseys</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=687&catalog_name=S2lkcw">Kids</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=702&catalog_name=TmlrZQ">Nike</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=653&catalog_name=T3V0ZXJ3ZWFy">Outerwear</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=652&catalog_name=UG9sb3M">Polos</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=681&catalog_name=U2hvcnRzL1BhbnRz">Shorts/Pants</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=705&catalog_name=U2FsZQ">Sale</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=647&catalog_name=U3dlYXRzaGlydHM">Sweatshirts</a></li><li><a href="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=645&catalog_name=VC1TaGlydHM">T-Shirts</a></li></ul>
</div><h2 class="menudivider">University Bookstore</h2>
    <ul>
		<li class="graduation"><a href="http://www.fortyninershops.net/site_graduation.asp?">Graduation Center</a></li>
        <li class="faculty"><a href="http://www.fortyninershops.net/site_faculty.asp?">Faculty Resource Center</a></li>
        <li class="copy"><a href="http://www.fortyninershops.net/site_campus_copy_center.asp?">Campus Copy Center</a></li>
  		<li class="returns"><a href="http://www.fortyninershops.net/site_return_guidelines.asp?">Return Guidelines</a></li>
  		<li class="rentguide"><a href="http://www.fortyninershops.net/site_rental_guidelines.asp?">Rental Guidelines</a></li>
  		<li class="shipping"><a href="http://www.fortyninershops.net/site_shipping_info_ut.asp?">Shipping Information</a></li>  
        <li class="promos"><a href="http://www.fortyninershops.net/site_promotions_events.asp?">Promotions &amp; Events</a></li>
    	<li class="hours"><a href="http://www.fortyninershops.net/site_hours.asp?">Hours of Operation</a></li>
    	<li class="contact"><a href="http://www.fortyninershops.net/site_contact_us.asp?">Contact Us</a></li>
	</ul>
    </div>
    <a name="content" id="content"></a>
    <div id="contain_column2">
					<div id="main">
				<a name="content"></a>
				
				<h1><span>9.49 Tees</span></h1><div class="breadcrumbs-cont"><p>You are here:</p><ul class="breadcrumbs"><li><a href="http://www.fortyninershops.net/shop_main.asp?">Home</a> �</li>
<li><a href="http://www.fortyninershops.net/shop_main.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc">Clothing</a> �</li>
<li>9.49 Tees</li>
</ul></div><div class="product-list clearfix"><div class="product-list-controls clearfix"><div class="product-list-paging">12 items<br/>page 1 of 1</div><div class="product-list-sort"><form action="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz" method="post" id="listsort_0"><label for="sort_0">sort items by</label><select name="sort" id="sort_0" onchange="submit()"><option value="0" selected="selected">name | A to Z</option><option value="1">name | Z to A</option><option value="2">price | low to high</option><option value="3">price | high to low</option></select></form></div></div>
<div class="products-row">
<div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6072&product_name=UHJvbW8gQ3N1bGIgQm9yZGVhdXggVGVlIENoYXI&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785971t.jpg" alt="Promo Csulb Bordeaux Tee Char" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6072&product_name=UHJvbW8gQ3N1bGIgQm9yZGVhdXggVGVlIENoYXI&type=1&target=shop_product_list.asp">Promo Csulb Bordeaux Tee Char</a></div><div class="product-price">$9.49</div></div><div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6073&product_name=UHJvbW8gQ3N1bGIgQm9yZGVhdXggVGVlIERyayBQdXI&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785964t.jpg" alt="Promo Csulb Bordeaux Tee Drk Pur" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6073&product_name=UHJvbW8gQ3N1bGIgQm9yZGVhdXggVGVlIERyayBQdXI&type=1&target=shop_product_list.asp">Promo Csulb Bordeaux Tee Drk Pur</a></div><div class="product-price">$9.49</div></div><div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6068&product_name=UHJvbW8gQ3N1bGIgSmFnZ2VyIFRlZSBDaGFy&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785865t.jpg" alt="Promo Csulb Jagger Tee Char" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6068&product_name=UHJvbW8gQ3N1bGIgSmFnZ2VyIFRlZSBDaGFy&type=1&target=shop_product_list.asp">Promo Csulb Jagger Tee Char</a></div><div class="product-price">$9.49</div></div></div><div class="products-row">
<div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6069&product_name=UHJvbW8gQ3N1bGIgSmFnZ2VyIFRlZSBOdnk&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785872t.jpg" alt="Promo Csulb Jagger Tee Nvy" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6069&product_name=UHJvbW8gQ3N1bGIgSmFnZ2VyIFRlZSBOdnk&type=1&target=shop_product_list.asp">Promo Csulb Jagger Tee Nvy</a></div><div class="product-price">$9.49</div></div><div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6077&product_name=UHJvbW8gQ3N1bGIgU2VhbCBCaWcgRG90cyBUZWUgQmxr&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785933t.jpg" alt="Promo Csulb Seal Big Dots Tee Blk" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6077&product_name=UHJvbW8gQ3N1bGIgU2VhbCBCaWcgRG90cyBUZWUgQmxr&type=1&target=shop_product_list.asp">Promo Csulb Seal Big Dots Tee Blk</a></div><div class="product-price">$9.49</div></div><div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6076&product_name=UHJvbW8gQ3N1bGIgU2VhbCBCaWcgRG90cyBUZWUgQmx1ZQ&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785926t.jpg" alt="Promo Csulb Seal Big Dots Tee Blue" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6076&product_name=UHJvbW8gQ3N1bGIgU2VhbCBCaWcgRG90cyBUZWUgQmx1ZQ&type=1&target=shop_product_list.asp">Promo Csulb Seal Big Dots Tee Blue</a></div><div class="product-price">$9.49</div></div></div><div class="products-row">
<div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6074&product_name=UHJvbW8gTGIgU3RhdGUgQm9yZGVyIFRlZSBCbGs&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785902t.jpg" alt="Promo Lb State Border Tee Blk" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6074&product_name=UHJvbW8gTGIgU3RhdGUgQm9yZGVyIFRlZSBCbGs&type=1&target=shop_product_list.asp">Promo Lb State Border Tee Blk</a></div><div class="product-price">$9.49</div></div><div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6075&product_name=UHJvbW8gTGIgU3RhdGUgQm9yZGVyIFRlZSBHcm4&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785919t.jpg" alt="Promo Lb State Border Tee Grn" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6075&product_name=UHJvbW8gTGIgU3RhdGUgQm9yZGVyIFRlZSBHcm4&type=1&target=shop_product_list.asp">Promo Lb State Border Tee Grn</a></div><div class="product-price">$9.49</div></div><div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6070&product_name=UHJvbW8gTGIgU3RhdGUgUGV0ZSBUZWUgR3J5&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785780t.jpg" alt="Promo Lb State Pete Tee Gry" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6070&product_name=UHJvbW8gTGIgU3RhdGUgUGV0ZSBUZWUgR3J5&type=1&target=shop_product_list.asp">Promo Lb State Pete Tee Gry</a></div><div class="product-price">$9.49</div></div></div><div class="products-row">
<div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6071&product_name=UHJvbW8gTGIgU3RhdGUgUGV0ZSBUZWUgTWFy&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785797t.jpg" alt="Promo Lb State Pete Tee Mar" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=6071&product_name=UHJvbW8gTGIgU3RhdGUgUGV0ZSBUZWUgTWFy&type=1&target=shop_product_list.asp">Promo Lb State Pete Tee Mar</a></div><div class="product-price">$9.49</div></div><div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=5951&product_name=UHJvbW8gTGJzdSBSZXN0cmFpbnQgVGVlIEJybg&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785773t.jpg" alt="Promo Lbsu Restraint Tee Brn" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=5951&product_name=UHJvbW8gTGJzdSBSZXN0cmFpbnQgVGVlIEJybg&type=1&target=shop_product_list.asp">Promo Lbsu Restraint Tee Brn</a></div><div class="product-price">$9.49</div></div><div class="product"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=5952&product_name=UHJvbW8gTGJzdSBSZXN0cmFpbnQgVGVlIEdsZA&type=1&target=shop_product_list.asp"><img src="http://www.fortyninershops.net/outerweb/product_images/12785766t.jpg" alt="Promo Lbsu Restraint Tee Gld" class="product-thumb" /></a><div class="product-name"><a href="http://www.fortyninershops.net/shop_product_detail.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz&pf_id=5952&product_name=UHJvbW8gTGJzdSBSZXN0cmFpbnQgVGVlIEdsZA&type=1&target=shop_product_list.asp">Promo Lbsu Restraint Tee Gld</a></div><div class="product-price">$9.49</div></div></div>
<div class="product-list-controls clearfix"><div class="product-list-paging">12 items<br/>page 1 of 1</div><div class="product-list-sort"><form action="http://www.fortyninershops.net/shop_product_list.asp?catalog_group_id=MzY&catalog_group_name=Q2xvdGhpbmc&catalog_id=704&catalog_name=OS40OSBUZWVz" method="post" id="listsort_1"><label for="sort_1">sort items by</label><select name="sort" id="sort_1" onchange="submit()"><option value="0" selected="selected">name | A to Z</option><option value="1">name | Z to A</option><option value="2">price | low to high</option><option value="3">price | high to low</option></select></form></div></div></div>
			</div>


    </div>
    <div id="contain_column3"></div>
<div class="clear_float"></div>
</div>
<div class="minheight"></div>
</div>
<div id="footer">
    <div id="contain_footer">
        <div id="contain_address">
            <div id="address_content">
                <address>
                <strong>Forty-Niner Shops Inc.</strong><br />
                    6049 East 7th Street<br />
                    Long Beach, CA 90840<br />
                    562.985.5093
                </address>
            </div>
        </div>
        <div id="contain_utilities">
            <ul>
              <li><a href="http://www.csulb.edu/aux/49ershops/corporate/privacy/" title="Terms of Use" target="_blank">Terms of Use</a></li>
            </ul>
        </div>
        <div class="clear_float"></div>
    </div>
</div>


</body>

</html>
